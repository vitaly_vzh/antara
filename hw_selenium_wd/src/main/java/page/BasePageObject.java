package page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePageObject {

    protected WebDriver driver;
    protected WebDriverWait wait5second;

    public BasePageObject(WebDriver driver) {
        this.driver = driver;
        this.wait5second = new WebDriverWait(driver, 5);
        PageFactory.initElements(driver, this);
    }
}
