package page.content.base;

import org.openqa.selenium.WebDriver;
import page.BasePageObject;
import page.OpenUrl;

public abstract class ContentBasePage extends BasePageObject implements OpenUrl {

    public ContentBasePage(WebDriver driver) {
        super(driver);
    }

    public BasePageObject openUrl(String url) {
        driver.get(url);
        return this;
    }
}
