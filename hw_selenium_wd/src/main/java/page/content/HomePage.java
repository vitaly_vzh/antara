package page.content;

import block.ResultBlock;
import block.SearchBlock;
import org.openqa.selenium.WebDriver;
import page.content.base.ContentBasePage;

public class HomePage extends ContentBasePage {

    protected SearchBlock searchBlock;
    protected ResultBlock resultBlock;

    public HomePage(WebDriver driver) {
        super(driver);
        this.searchBlock = new SearchBlock(driver);
        this.resultBlock = new ResultBlock(driver);
    }

    public SearchBlock getSearchBlock() {
        return searchBlock;
    }

    public ResultBlock getResultBlock() {
        return resultBlock;
    }

    @Override
    public HomePage openUrl() {
        super.openUrl("https://www.avito.ru/");
        return this;
    }
}
