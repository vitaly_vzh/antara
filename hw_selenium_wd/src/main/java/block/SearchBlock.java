package block;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import page.BasePageObject;
import page.content.HomePage;

import java.util.HashMap;
import java.util.Map;

public class SearchBlock extends BasePageObject {

    @FindBy(css = "[id='category']")
    private WebElement category;

    @FindBy(xpath = "//input[contains(@id, 'downshift')]")
    private WebElement search;

    @FindBy(xpath = "//div[@data-marker='search-form/region']/div")
    private WebElement city;

    @FindBy(xpath = "//div[@data-marker='search-form/radius']/div")
    private WebElement radius;

    @FindBy(xpath = "//button[@data-marker='search-form/submit-button']")
    private WebElement buttonSearch;

    @FindBy(xpath = "//input[@data-marker='popup-location/region/input']")
    private WebElement pupUpMenuSelectCity;

    @FindBy(xpath = "//span/strong[text() = 'Владивосток']")
    private WebElement dropDownListCityFirstPosition;

    @FindBy(xpath = "//button[@data-marker='popup-location/save-button']")
    private WebElement buttonShow;

    public SearchBlock selectCategory(String text) {
        Select select = new Select(category);
        String value = "";
        Map<String, String> webElements = new HashMap<>();

        select.getOptions().forEach(option -> {
            webElements.put(option.getAttribute("value"), option.getText().toLowerCase());
        });

        for (Map.Entry<String, String> entry : webElements.entrySet()) {
            if (entry.getValue().contains(text.toLowerCase())) {
                value = entry.getKey();
                break;
            }
        }
        select.selectByValue(value);
        return this;
    }

    public HomePage selectCity(String text) {
        city.click();
        wait5second.until(ExpectedConditions.visibilityOf(pupUpMenuSelectCity));
        pupUpMenuSelectCity.sendKeys(text);
        wait5second.until(ExpectedConditions.textToBePresentInElement(dropDownListCityFirstPosition, text));
        dropDownListCityFirstPosition.click();
        buttonShow.click();
        return new HomePage(driver);
    }

    public SearchBlock searchText(String text) {
        search.sendKeys(text);
        return this;
    }

    public SearchBlock clickSearchButton() {
        buttonSearch.click();
        return this;
    }

    public SearchBlock(WebDriver driver) {
        super(driver);
    }
}
