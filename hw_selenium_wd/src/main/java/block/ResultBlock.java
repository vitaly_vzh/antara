package block;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import page.BasePageObject;
import page.content.HomePage;
import utils.Utils;

import java.util.List;

public class ResultBlock extends BasePageObject {

    @FindBy(xpath = "//span[@data-marker='delivery-filter/text']")
    private WebElement checkboxDelivery;

    @FindBy(xpath = "//button[@data-marker='search-filters/submit-button']")
    private WebElement buttonShowResults;

    @FindBy(xpath = "//div[contains(@class, 'index-content')]//select")
    private WebElement dropDownListFilters;

    @FindBy(xpath = "//a[@data-marker='item-title']/h3")
    private List<WebElement> positionsName;

    @FindBy(xpath = "//span[@data-marker='item-price']/span")
    private List<WebElement> cost;

    public ResultBlock clickCheckboxDelivery() {
        Utils.scroll(driver, checkboxDelivery);

        if (!checkboxDelivery.isSelected()) {
            checkboxDelivery.click();
        }
        return this;
    }

    public ResultBlock clickShowResultButton() {
        Utils.scroll(driver, buttonShowResults);

        buttonShowResults.click();
        return this;
    }

    public ResultBlock clickFiltersList(String text) {
        Select select = new Select(dropDownListFilters);
        select.selectByVisibleText(text);
        return this;
    }

    public HomePage checkNameAndCost(int numberPositions) {
        try {
            for (int i = 0; i < numberPositions; i++) {
                System.out.print(positionsName.get(i).getText() + ": ");
                System.out.println(cost.get(i).getText());
            }
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Вы ввели большее число, чем количество найденных позиций = " + cost.size() + 1);
        }
        return new HomePage(driver);
    }

    public ResultBlock(WebDriver driver) {
        super(driver);
    }
}
