import base.BeforeAndAfter;
import org.testng.annotations.Test;
import page.content.HomePage;

public class SearchTest extends BeforeAndAfter {

    @Test
    public void searchPrinterInVladivostok() {

        new HomePage(driver)
                .openUrl()
                .getSearchBlock()
                .selectCategory("Оргтехника и расходники")
                .searchText("Принтер")
                .clickSearchButton()
                .selectCity("Владивосток")
                .getResultBlock()
                .clickCheckboxDelivery()
                .clickShowResultButton()
                .clickFiltersList("Дороже")
                .checkNameAndCost(3);
    }
}
