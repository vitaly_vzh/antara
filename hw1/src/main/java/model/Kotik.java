package model;

public class Kotik {

    private static int countOfInstance = 0;
    private static int countOfIteration = 24;
    private static int count = 0;
    private int satiety;
    private String name;
    private String meow;
    private int prettiness;
    private int weight;
    private String nameOfFeed;

    public Kotik() {
        countOfInstance++;
    }

    public Kotik(String name, int prettiness, int weight, String meow) {
        countOfInstance++;
        this.name = name;
        this.prettiness = prettiness;
        this.weight = weight;
        this.meow = meow;
    }

    public boolean play() {
        if (!hungry()) {
            satiety--;
            count++;
            System.out.println(name + " играет");
            return true;
        } else {
            System.out.println(" и не играет");
            return false;
        }
    }

    public boolean sleep() {
        if (!hungry()) {
            satiety--;
            count++;
            System.out.println(name + " спит");
            return true;
        } else {
            System.out.println(" и не спит");
            return false;
        }
    }

    public boolean chaseMouse() {
        if (!hungry()) {
            satiety--;
            count++;
            System.out.println(name + " гоняется за мышью");
            return true;
        } else {
            System.out.println(" и не гоняется за мышью");
            return false;
        }
    }

    public boolean walk() {
        if (!hungry()) {
            satiety--;
            count++;
            System.out.println(name + " гуляет");
            return true;
        } else {
            System.out.println(" и не гуляет");
            return false;
        }
    }

    public boolean jump() {
        if (!hungry()) {
            satiety--;
            count++;
            System.out.println(name + " прыгает");
            return true;
        } else {
            System.out.println(" и не прыгает");
            return false;
        }
    }

    private boolean hungry() {
        if (satiety == 0) {
            count++;
            System.out.print(name + " хочет кушать");
            return true;
        }
        return false;
    }

    private void eat(int feed) {
        satiety += feed;
        if (count < 24) {
            System.out.println("\\_/ " + name + " покормлен");
        }
        count++;
        countOfIteration--;
    }

    private void eat(int feed, String nameOfFeed) {
        satiety += feed;
        if (count < 24) {
            System.out.println("\\_/ " + name + " покормлен кормом " + nameOfFeed);
        }
        count++;
        countOfIteration--;
    }

    private void eat() {
        eat(getRandomOfFeed(), nameOfFeed);
    }

    public void liveAnotherDay() {
        for (int i = 0; i < countOfIteration; i++) {

            switch (getRandomNumber()) {
                case (1):
                    if (!chaseMouse()) {
                        eat(getRandomOfFeed());
                    }
                    break;
                case (2):
                    if (!jump()) {
                        eat(getRandomOfFeed());
                    }
                    break;
                case (3):
                    if (!play()) {
                        eat(getRandomOfFeed());
                    }
                    break;
                case (4):
                    if (!sleep()) {
                        eat(getRandomOfFeed());
                    }
                    break;
                case (5):
                    if (!walk()) {
                        eat(getRandomOfFeed());
                    }
                    break;
            }
        }
    }

    private static int getRandomNumber() {
        return (int) (Math.random() * 5) + 1;
    }

    private static int getRandomOfFeed() {
        return (int) (Math.random() * 10) + 1;
    }

    public void setSatiety() {
        satiety = (int) (Math.random() * 24) + 1;
    }

    public void setKotik(String name, int prettiness, int weight, String meow) {
        this.name = name;
        this.prettiness = prettiness;
        this.weight = weight;
        this.meow = meow;
    }

    public String getName() {
        return name;
    }

    public String getMeow() {
        return meow;
    }

    public int getPrettiness() {
        return prettiness;
    }

    public int getWeight() {
        return weight;
    }

    public void setNameOfFeed(String nameOfFeed) {
        this.nameOfFeed = nameOfFeed;
    }

    public static int getCountOfInstance() {
        return countOfInstance;
    }
}
