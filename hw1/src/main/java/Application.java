import model.Kotik;

public class Application {

    public static void main(String[] args) {

        Kotik cat1 = new Kotik("Барсик", 5, 8, "Мяууу");
        cat1.setSatiety();

        Kotik cat2 = new Kotik();
        cat2.setKotik("Васька", 7, 5, "Мууур");
        cat2.setSatiety();
        cat2.setNameOfFeed("Whiskas");

        System.out.println("Котик " + cat1.getName() + ", весом: " + cat1.getWeight() + " кг.");
        System.out.println("-----------------");

        cat1.liveAnotherDay();
        System.out.println("-----------------");

        if (cat1.getMeow().equals(cat2.getMeow())) {
            System.out.println("Котики разговаривают одинаково");
        } else {
            System.out.println("Котики разговаривают по разному");
        }

        System.out.println("-----------------");

        System.out.println("Было создано: " + Kotik.getCountOfInstance() + " экземпляра класса (котика)");
    }
}
