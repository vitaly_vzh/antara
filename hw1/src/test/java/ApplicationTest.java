import model.Kotik;
import org.junit.jupiter.api.*;

@DisplayName("Тесты")

class ApplicationTest {
    
    private static int expectedInstance = 0;

    @BeforeEach
    void beforeEach() {
        expectedInstance++;
    }

    @DisplayName("Количество созданных экземпляров класса")
    @Test
    void testCountOfInstance() {
        Kotik kotik = new Kotik("Max", 3, 9, "Мяу");

        Assertions.assertEquals(expectedInstance, Kotik.getCountOfInstance());
    }

    @DisplayName("Провека на пустое name")
    @Test
    void testNameNotNull() {
        Kotik kotik = new Kotik("Max", 3, 9, "Мяу");

        Assertions.assertNotNull(kotik.getName());
    }

    @DisplayName("Провека на пустое prettiness")
    @Test
    void testPrettinessNotNull() {
        Kotik kotik = new Kotik("Max", 3, 9, "Мяу");

        Assertions.assertNotNull(kotik.getPrettiness());
    }

    @DisplayName("Провека на пустое weight")
    @Test
    void testWeightNotNull() {
        Kotik kotik = new Kotik("Max", 3, 9, "Мяу");

        Assertions.assertNotNull(kotik.getWeight());
    }

    @DisplayName("Провека на пустое meow")
    @Test
    void testMeowNotNull() {
        Kotik kotik = new Kotik("Max", 3, 9, "Мяу");

        Assertions.assertNotNull(kotik.getMeow());
    }
}