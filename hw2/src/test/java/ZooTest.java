import animals.*;
import food.Food;
import food.Meat;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ZooTest {

    @Test
    void positiveSatiety() {
        Duck duck = new Duck(3, 5, "Утка");

        Assertions.assertTrue(duck.getSatiety() >= 0);
    }

    @Test
    void positiveMaxSatiety() {
        Duck duck = new Duck(3, 5, "Утка");

        Assertions.assertTrue(duck.getMaxSatiety() >= 0);
    }

    @Test
    void positiveMeatFood() {
        Food meatFood = new Meat(5);

        Assertions.assertTrue(meatFood.getAmountOfFeed()>= 0);
    }

    @Test
    void positiveGrassFood() {
        Food grassFood = new Meat(15);

        Assertions.assertTrue(grassFood.getAmountOfFeed()>= 0);
    }
}