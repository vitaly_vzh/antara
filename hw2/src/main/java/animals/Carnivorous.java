package animals;

import food.Food;
import food.Grass;
import food.Meat;

public class Carnivorous extends Animal {

    public Carnivorous(int satiety, int maxSatiety, String name) {
        super(satiety, maxSatiety, name);
    }

    @Override
    public void eat(Food food) {
        if (food instanceof Grass) {
            System.out.println(getName() + " не ест траву!");
            setResult(false);
        } else if (food instanceof Meat) {
            System.out.println(getName() + " ест мясо!");
            setResult(true);
        }
    }
}
