package animals;

public class Elephant extends Herbivore implements Run, Swim, Voice {

    public Elephant(int satiety, int maxSatiety, String name) {
        super(satiety, maxSatiety, name);
    }

    @Override
    public void run() {
        System.out.println("Бегает как " + getName());
    }

    @Override
    public void swim() {
        System.out.println("Плавает как " + getName());
    }

    @Override
    public String voice() {
        return getName() + " кричит: У-уууу-уу";
    }
}
