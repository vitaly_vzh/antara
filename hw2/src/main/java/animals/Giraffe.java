package animals;

public class Giraffe extends Herbivore  implements Run, Voice {

    public Giraffe(int satiety, int maxSatiety, String name) {
        super(satiety, maxSatiety, name);
    }

    @Override
    public void run() {
        System.out.println("Бегает как " + getName());
    }

    @Override
    public String voice() {
        return getName() + " кричит: Раааа-аааа-о";
    }
}
