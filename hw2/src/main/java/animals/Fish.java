package animals;

public class Fish extends Herbivore implements Swim {

    public Fish(int satiety, int maxSatiety, String name) {
        super(satiety, maxSatiety, name);
    }

    @Override
    public void swim() {
        System.out.println("Плавает как " + getName());
    }
}
