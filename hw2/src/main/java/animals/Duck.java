package animals;

public class Duck extends Carnivorous implements Run, Swim, Fly, Voice {

    public Duck(int satiety, int maxSatiety, String name) {
        super(satiety, maxSatiety, name);
    }

    @Override
    public void fly() {
        System.out.println("Летает как " + getName());
    }

    @Override
    public void run() {
        System.out.println("Бегает как " + getName());
    }

    @Override
    public void swim() {
        System.out.println("Плавает как " + getName());
    }

    @Override
    public String voice() {
        return getName() + " кричит: Кря-кря";
    }
}
