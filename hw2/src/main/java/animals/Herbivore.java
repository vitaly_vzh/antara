package animals;

import food.Food;
import food.Grass;
import food.Meat;

public class Herbivore extends Animal {

    public Herbivore(int satiety, int maxSatiety, String name) {
        super(satiety, maxSatiety, name);
    }

    @Override
    public void eat(Food food) {
        if (food instanceof Meat) {
            System.out.println(getName() + " не ест мясо!");
            setResult(false);
        } else if (food instanceof Grass) {
            System.out.println(getName() + " ест траву!");
            setResult(true);
        }
    }
}
