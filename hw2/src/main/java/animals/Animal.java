package animals;

import food.Food;

public abstract class Animal {

    private int satiety;
    private int maxSatiety;
    private String name;
    private boolean result;

    public Animal(int satiety, int maxSatiety, String name) {
        this.satiety = satiety;
        this.maxSatiety = maxSatiety;
        this.name = name;
    }

    public abstract void eat(Food food);

    public int getSatiety() {
        return satiety;
    }

    public void setSatiety(int satiety) {
        this.satiety = satiety;
    }

    public int getMaxSatiety() {
        return maxSatiety;
    }

    public String getName() {
        return name;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }
}
