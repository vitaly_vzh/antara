package animals;

public class Wolf extends Carnivorous  implements Run, Swim, Voice {

    public Wolf(int satiety, int maxSatiety, String name) {
        super(satiety, maxSatiety, name);
    }

    @Override
    public void run() {
        System.out.println("Бегает как " + getName());
    }

    @Override
    public void swim() {
        System.out.println("Плавает как " + getName());
    }

    @Override
    public String voice() {
        return getName() + " воет: Уууууууууу";
    }
}
