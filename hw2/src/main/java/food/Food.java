package food;

public abstract class Food {

    private int amountOfFeed;

    public Food(int amountOfFeed) {
        this.amountOfFeed = amountOfFeed;
    }

    public int getAmountOfFeed() {
        return amountOfFeed;
    }
}
