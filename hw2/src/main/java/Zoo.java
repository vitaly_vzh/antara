import animals.*;
import food.Food;
import food.Grass;
import food.Meat;
import animals.Swim;
import zoo.Worker;

import java.util.Arrays;
import java.util.List;

public class Zoo {

    public static void main(String[] args) {

        Duck duck = new Duck(3, 5, "Утка");
        Elephant elephant = new Elephant(10, 50,"Слон");
        Fish fish = new Fish(2, 4,"Рыба");
        Fox fox = new Fox(4, 10,"Лиса");
        Giraffe giraffe = new Giraffe(4, 15, "Жираф");
        Wolf wolf = new Wolf(0, 20, "Волк");
        Food meatFood = new Meat(5);
        Food grassFood = new Grass(15);
        Worker worker = new Worker();

        List<Animal> animalList = Arrays.asList(duck, elephant, fox, giraffe, wolf, fish);
        List<Voice> animalVoice = Arrays.asList(duck, elephant, fox, giraffe, wolf);
//        List<Voice> errorVoice = Arrays.asList(fish); // ошибка компиляции при передаче "немых" экземплятов класса
        List<Swim> pond = Arrays.asList(duck, elephant, fish, fox, wolf);

        for(Animal animal : animalList) {
            worker.feed(animal, meatFood);
            worker.feed(animal, grassFood);
            System.out.println("-----------");
        }

        for(Voice voice : animalVoice) {
            worker.getVoice(voice);
        }

        System.out.println("-----------");

        for(Swim swim : pond) {
            swim.swim();
        }
    }
}
