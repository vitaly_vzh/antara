package zoo;

import animals.Animal;
import animals.Voice;
import food.Food;

public class Worker {

    public void feed(Animal animal, Food food) {

        animal.eat(food);
        animal.setSatiety(Math.min(animal.getSatiety() + food.getAmountOfFeed(), animal.getMaxSatiety()));

        if (animal.isResult() && animal.getSatiety() >= animal.getMaxSatiety()) {
            System.out.println(animal.getName() + " больше не хочет есть!");
            System.out.println("Уровень сытости: " + animal.getSatiety() + " из " + animal.getMaxSatiety());
        } else if (animal.isResult()){
            System.out.println(animal.getName() + " хочет еще поесть!");
            System.out.println("Уровень сытости: " + animal.getSatiety() + " из " + animal.getMaxSatiety());
        }
    }

    public void getVoice(Voice voice) {
            System.out.println(voice.voice());
    }
}
